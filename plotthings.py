#!/usr/bin/env python

########################################################################
## Plotting code written for Phantom Output
## Current version made for plotting white dwarf stars
##
## Author: Alex Bercik & Terrence Tricco, May 2018
########################################################################

import matplotlib.pyplot as plt
import numpy as np
from scipy import optimize as opt
import argparse
import traceback
import sys
from libanalysis import PhantomAnalysis as pa

## Parse CommandLine Input
parser = argparse.ArgumentParser(
         description='Display data from Phantom dump file',
         epilog='Written by Alex Bercik & Terrence Tricco May 2018')
parser.add_argument('dumpfile',help='Desired dumpfile, ex. blast_00000')
parser.add_argument('x_axis',help='What to plot on the x-axis. [m]ass enclosed,'\
			'[r]adius')
parser.add_argument('y_axis',help='What to plot on the y-axis. [d]ensity,'\
                    '[t]emperature')
parser.add_argument('-r','--remove',choices=['z','x','y','za','xa','ya','charles'],
			help='remove points except on a particular plane or axis. [z]plane, [x]plane,'\
			' [y]plane, [za]xis, [xa]xis, [ya]xis')
parser.add_argument('-d','--seconddump',help='Desired second dumpfile, ex. blast_00000')
args = parser.parse_args()

###### TO DO: ADD MORE POSSIBLE THINGS TO PLOT
###### TO DO: ADD OPTIONAL ARGUMENTS TO LIMIT PLOT
###### TO DO: Make it work for small dumps?
###### TO DO: Add in magnetic field (dump.bxyz)? SEE plotemag.py
###### TO DO: Currently hard coded in to ignore magnetic field in phantomanalysis.py

## Read information from dumpfile into python using PhantomAnalysis package
## (be sure to compile libphantom library before running script)
try:
	dumpfile = args.dumpfile
	dump = pa(dumpfile)
except:
	error = traceback.format_exc()
	print '\n[ERROR]: {0}'.format(error)
	sys.exit()

## Extract information from dumpfile
try:
	mass = dump.massofgas
	time = dump.time
	numparticles = dump.npart
	xpositions = dump.xyzh[0]
	ypositions = dump.xyzh[1]
	zpositions = dump.xyzh[2]
	smoothinglengths = dump.xyzh[3]
	#xvelocities = dump.vxyz[0]
	#yvelocities = dump.vxyz[1]
	#zvelocities = dump.vxyz[2]
	#thermenergies = dump.utherm
	temperatures = dump.temperature
except:
	error = traceback.format_exc()
	print '\n[ERROR]: {0}'.format(error)
	sys.exit()

if args.seconddump:
	try:
		dumpfile3 = args.seconddump
		dump3 = pa(dumpfile3)
	except:
		error = traceback.format_exc()
		print '\n[ERROR]: {0}'.format(error)
		sys.exit()

	## Extract information from dumpfile
	try:
		numparticles3 = dump3.npart
		xpositions3 = dump3.xyzh[0]
		ypositions3 = dump3.xyzh[1]
		zpositions3 = dump3.xyzh[2]
		smoothinglengths3 = dump3.xyzh[3]
		#xvelocities3 = dump3.vxyz[0]
		#yvelocities3 = dump3.vxyz[1]
		#zvelocities3 = dump3.vxyz[2]
		#thermenergies3 = dump3.utherm
		temperatures3 = dump3.temperature
	except:
		error = traceback.format_exc()
		print '\n[ERROR]: {0}'.format(error)
		sys.exit()

	
## Calculate radius
radii = np.sqrt(xpositions**2 + ypositions**2 + zpositions**2)
if args.seconddump: radii3 = np.sqrt(xpositions3**2 + ypositions3**2 + zpositions3**2)


## Calculate densities
### TO DO: ADD OPTION TO CHANGE THESE RATHER THAN HARD-CODING THEM IN (Mass per particle should be easy)
hfact = 1.2
densities = mass * hfact**3 / smoothinglengths**3
if args.seconddump: densities3 = mass * hfact**3 / smoothinglengths3**3
## Convert code units (solar mass / solar radii) to g/cm^3
densities = densities * 5.901 ## NOTE: conversion given by code. Wolfram has differing value of 5.905347
if args.seconddump: densities3 = densities3 * 5.901
## check accuracy
print 'CHECK: Total Mass = mass_per_particle * num_particles = {0}'.format(str(mass*numparticles))

#######################################################################
## If indicated, remove all points outside either a small disk around z, x, or y	
## or remove all points outside a small cylinder around the z, x, or y axes
## The actual removal is done when adding particles to bins. Here we simply calculate the tolerance
if args.remove:
	## To find the desired range from which to remove particles, fit a gaussian to densities along the z axis
	## The maximum tolerance will be 1/2 of the characteristic width of the fitted gaussian
	
	def gauss(z,sig,mu,a):
		return a*np.exp(-np.power(z-mu, 2.)/(2*np.power(sig, 2.)))
		
	## Define cylindrical radii to restrict positions to a small cylinder around the z axis
	cylindricalradii = np.sqrt(xpositions**2 + ypositions**2)
	
	## Define new containers to hold z densities and positions only for particles within small cylinder
	zdensities_axis = []
	zpositions_axis = []
	for i in range(numparticles):
		if np.abs(cylindricalradii[i]) < 0.001:
			zdensities_axis.append(densities[i])
			zpositions_axis.append(zpositions[i])
			
	## Sort the z positions and radii in ascending order to make the fitting easier
	sortedindices = np.argsort(zpositions_axis)
	## Make empty lists to store new sorted values
	sortedzpositions_axis = np.zeros(len(zpositions_axis))	
	sortedzdensities_axis = np.zeros(len(zpositions_axis))
	## Iterate through lists and put each value into corresponding index
	for i in range(len(zpositions_axis)):
		sortedzpositions_axis[i] = zpositions_axis[sortedindices[i]]
		sortedzdensities_axis[i] = zdensities_axis[sortedindices[i]]
	
	## Fit the data to a gaussian distribution
	sig, mu, a = opt.curve_fit(gauss, sortedzpositions_axis, sortedzdensities_axis, (0.005,0.0,1E7))[0]
	## Take our tolerance as half of the standard deviation (characteristic width)
	tolerance = sig/2.
	print 'Omitting data points outside desired range. Tolerance = {0} (Solar Radii)'.format(tolerance)
	
	## Plot Fitted Gaussian to verify a good fit
	plt.figure(1)
	plt.rc('text', usetex=True)
	plt.rc('font', family='serif')
	plt.title(r'Fitted Gaussian')
	plt.xlabel(r'$z$ position (Solar Radii)')
	plt.ylabel(r'$\rho$ (g $\text{cm}^{-3}$)')
	plt.plot(zpositions_axis, zdensities_axis, '.', color='g')
	plt.plot(sortedzpositions_axis, gauss(sortedzpositions_axis, sig, mu, a), color='r')


if args.seconddump and args.remove:
       
	cylindricalradii3 = np.sqrt(xpositions3**2 + ypositions3**2)

	zdensities3_axis = []
	zpositions3_axis = []
	for i in range(numparticles3):
		if np.abs(cylindricalradii3[i]) < 0.001:
			zdensities3_axis.append(densities3[i])
			zpositions3_axis.append(zpositions3[i])

	## Sort the z positions and radii in ascending order to make the fitting easier
	sortedindices3 = np.argsort(zpositions3_axis)
	## Make empty lists to store new sorted values
	sortedzpositions3_axis = np.zeros(len(zpositions3_axis))
	sortedzdensities3_axis = np.zeros(len(zpositions3_axis))
	## Iterate through lists and put each value into corresponding index
	for i in range(len(zpositions3_axis)):
		sortedzpositions3_axis[i] = zpositions3_axis[sortedindices3[i]]
		sortedzdensities3_axis[i] = zdensities3_axis[sortedindices3[i]]

	## Fit the data to a gaussian distribution
	sig3, mu3, a3 = opt.curve_fit(gauss, sortedzpositions3_axis, sortedzdensities3_axis, (0.005,0.0,1E7))[0]
	## Take our tolerance as half of the standard deviation (characteristic width)
	tolerance3 = sig3/2.
	print 'Tolerance for second dumpfile:'
	print 'Omitting data points outside desired range. Second Tolerance = {0} (Solar Radii)'.format(tolerance3)

	## Plot Fitted Gaussian to verify a good fit
	plt.figure(3)
	plt.rc('text', usetex=True)
	plt.rc('font', family='serif')
	plt.title(r'Fitted Gaussian')
	plt.xlabel(r'$z$ position (Solar Radii)')
	plt.ylabel(r'$\rho$ (g $\text{cm}^{-3}$)')
	plt.plot(zpositions3_axis, zdensities3_axis, '.', color='g')
	plt.plot(sortedzpositions3_axis, gauss(sortedzpositions3_axis, sig3, mu3, a3), color='r')


####### TO DO: Add option for manual input of tolerance

## Bin the radii, currently in 1000 bins, increase for higher resolution
## We will average y values in each bin
bins = np.linspace(0.0, max(radii),num=1000)
hist, bin_edges = np.histogram(radii, bins)
massperbin = hist*mass
enclosedmass = np.array(np.cumsum(massperbin))
## NOTE: enclosedmass is an array of the total enclosed mass as a function of radius 
##       from 0 to max(radius) in increments determined by the histogram
## Check accuracy
print 'CHECK: Total Enclosed Mass = {0}'.format(str(enclosedmass[-1]))

## Set appropriate y values
if args.y_axis == 'd':
	yvalues = densities
elif args.y_axis == 't':
	yvalues = temperatures
else:
	print '\n[ERROR]: Unknown Argument for y Value.'
	sys.exit()
	
## Reproduce `histogram` binning by manually shifting the 
## rightmost bin edge by an epsilon value to include max value in upper bin:
bin_edges[-1] += 10**(-6)
digitizedradii = np.digitize(radii, bin_edges)
## For each particle's (index) y value, get corresponding digitized bin value, and add the y value
## of the particle to that digitized index of a new multi-dimensional array of size the same as bin number
binnedyvalues = [[] for x in range(len(bin_edges)-1)]
binnedyvalues2 = [[] for x in range(len(bin_edges)-1)]
n=0
n2=0
print 'Checking each particle...'
for i in range(numparticles):
	############ Before proceeding, check if we want to restrict data to a cross-section or axis ###############
	if args.remove: 
		## Only add the particles to the bin if they are within a desired tolerance (calculated at start of file)
		## Check whether to restrict to a plane or axis, set the checking value accordingly
		if args.remove == 'x':
			checkpoints = xpositions
		elif args.remove == 'y':
			checkpoints = ypositions	
		elif args.remove == 'z':
			checkpoints = zpositions
		elif args.remove == 'xa':
			checkpoints = np.sqrt(ypositions**2 + zpositions**2)
		elif args.remove == 'ya':
			checkpoints = np.sqrt(xpositions**2 + zpositions**2)
		elif args.remove == 'za':
			checkpoints = cylindricalradii
		elif args.remove == 'charles':
			checkpoints = zpositions
			checkpoints2 = cylindricalradii
				
		## Add particle's y value to the corresponding bin only if it is within the desired tolerance. Otherwise discard it.
		if np.abs(checkpoints[i]) < tolerance:
			binnedyvalues[digitizedradii[i]-1].append(yvalues[i])
		else: n=n+1

		# Do this part only if we are plotting a second quantity
		if args.remove == 'charles':
			if np.abs(checkpoints2[i]) < tolerance:
				binnedyvalues2[digitizedradii[i]-1].append(yvalues[i])
			else: n2=n2+1

	## If we are not restricting the data points, simply add them to the corresponding bins
	else: binnedyvalues[digitizedradii[i]-1].append(yvalues[i])
	
	if i == int(numparticles/4): print '25% complete'
	if i == int(numparticles/2): print '50% complete'
	if i == int(3*numparticles/4): print '75% complete'
	
	##############################################################################################################

if args.remove: print 'Omitted {0} particles of {1}'.format(n,numparticles)		
print 'Averaging Bins...'

## We now have the y values grouped in the same order as the binned radii. Average the bins to get average
## y value for each bin, something we can plot against enclosed mass at each bin
binnedyvalueserror = [[],[]]
for i in range(len(binnedyvalues)):
	## Mask empty array values to make a smooth curve
	if np.array(binnedyvalues[i]).any():
		averageyvaluei = np.average(binnedyvalues[i])
		binnedyvalueserror[0].append(averageyvaluei - min(binnedyvalues[i]))
		binnedyvalueserror[1].append(max(binnedyvalues[i]) - averageyvaluei)
		binnedyvalues[i] = averageyvaluei
	else:
		binnedyvalues[i] = None
		binnedyvalueserror[0].append(None)
		binnedyvalueserror[1].append(None)
binnedyvalues = np.array(binnedyvalues)
binnedyvalueserror = np.array(binnedyvalueserror)
mask = np.isfinite(binnedyvalues.astype(np.double))

# Do the same if we are plotting a second quantity
if args.remove == 'charles':
	binnedyvalueserror2 = [[],[]]
	for i in range(len(binnedyvalues2)):
		## Mask empty array values to make a smooth curve
		if np.array(binnedyvalues2[i]).any():
			averageyvaluei2 = np.average(binnedyvalues2[i])
			binnedyvalueserror2[0].append(averageyvaluei2 - min(binnedyvalues2[i]))
			binnedyvalueserror2[1].append(max(binnedyvalues2[i]) - averageyvaluei2)
			binnedyvalues2[i] = averageyvaluei2
		else:
			binnedyvalues2[i] = None
			binnedyvalueserror2[0].append(None)
			binnedyvalueserror2[1].append(None)
	binnedyvalues2 = np.array(binnedyvalues2)
	binnedyvalueserror2 = np.array(binnedyvalueserror2)
	mask2 = np.isfinite(binnedyvalues2.astype(np.double))


	
## TO DO: Use stats module to caluclate standard deviation instead of just max/min? Or a percentile?
## TO DO: Add this graph with some flag to a multi-plot with subplots

print 'Plotting...'
	
## Now determine what to plot on the x axis, radii or enclosed mass
## if mass, use enclosed mass at the right bin edges for x
## if radii, use the average between the bin edge to the left or right
## TO DO: Add possibilities for subplots with some flag 
if args.x_axis == 'm':	
	## TO DO: Use stats module to caluclate standard deviation instead of just max/min? Or a percentile?
		
	## Plot Enclosed Mass against y value (Density or Temperature)
	plt.figure(2)
	plt.rc('text', usetex=True)
	plt.rc('font', family='serif')
	## TO DO: Add possibility to change units
	if args.remove == 'charles':
		plt.plot(enclosedmass[mask]/enclosedmass[-1], binnedyvalues[mask], color='blue', label='With Shock')
		plt.plot(enclosedmass[mask2]/enclosedmass[-1], binnedyvalues2[mask2], color='blue', linestyle='--')
	else:
		plt.errorbar(enclosedmass[mask]/enclosedmass[-1], binnedyvalues[mask], yerr=[binnedyvalueserror[0][mask],binnedyvalueserror[1][mask]], 
				capsize=3, color='black')
	if args.y_axis == 'd':
		plt.title(r'Density against Enclosed Mass t={0}'.format(time))
		plt.ylabel(r'$\rho$ (g $\text{cm}^{-3}$)')
		plt.yscale('log')	
		plt.ylim(ymin=1E3)
		#plt.ylim(ymax=5E7)
	elif args.y_axis == 't':
		plt.title(r'Temperature against Enclosed Mass t={0}'.format(time)) 
		plt.ylabel(r'T (K)')
		plt.yscale('log')
		plt.ylim(ymin=1E5)
		#plt.ylim(ymax=9E8)	
	plt.xlabel(r'$M/M_{\text{tot}}$')

elif args.x_axis == 'r':
	## Calculate average radii in each bin
	avgedges = []
	for i in range(len(bins)-1):
		avgedges = np.append(avgedges, (bins[i]+bins[i+1])/2.)
	plt.figure(2)
	plt.rc('text', usetex=True)
	plt.rc('font', family='serif')
	## TO DO: Add possibility to change units
	## TO DO: Add label that distinguishes whether ommitting or not
        if args.remove == 'charles':
                plt.plot(avgedges[mask], binnedyvalues[mask], color='blue', label='With Shock')
                plt.plot(avgedges[mask2], binnedyvalues2[mask2], color='blue', linestyle='--')
        else:
             	plt.errorbar(avgedges[mask], binnedyvalues[mask], yerr=[binnedyvalueserror[0][mask],binnedyvalueserror[1][mask]],
                                capsize=3, color='black')
	if args.y_axis == 'd':
		plt.title(r'Density against Radius t={0}'.format(time))
		plt.ylabel(r'$\rho$ (g $\text{cm}^{-3}$)')
		plt.yscale('log')	
		plt.ylim(ymin=1)
	elif args.y_axis == 't':
		plt.title(r'Temperature against Radius t={0}'.format(time)) 
		plt.ylabel(r'T (K)')
		plt.yscale('log')
		plt.ylim(ymin=100)	
	plt.xlabel(r'$r$ (Solar Radii)')
		
	
## TO DO: REDO BY BINNING MASS WITH GRAVITATIONAL POTENTIAL	
	
	
	
##################################################################################################################################	
##################################################################################################################################	
### Do again for second dump
if args.seconddump:
	print 'DOING AGAIN FOR SECOND DUMPFILE'
	bins3 = np.linspace(0.0, max(radii3),num=1000)
	hist3, bin_edges3 = np.histogram(radii3, bins3)
	massperbin3 = hist3*mass
	enclosedmass3 = np.array(np.cumsum(massperbin3))
	## NOTE: enclosedmass is an array of the total enclosed mass as a function of radius
	##	 from 0 to max(radius) in increments determined by the histogram
	## Check accuracy
	print 'CHECK: Total Enclosed Mass = {0}'.format(str(enclosedmass3[-1]))
	
	## Set appropriate y values
	if args.y_axis == 'd':
	        yvalues3 = densities3
	elif args.y_axis == 't':
	        yvalues3 = temperatures3
	
	## Reproduce `histogram` binning by manually shifting the
	## rightmost bin edge by an epsilon value to include max value in upper bin:
	bin_edges3[-1] += 10**(-6)
	digitizedradii3 = np.digitize(radii3, bin_edges3)
	## For each particle's (index) y value, get corresponding digitized bin value, and add the y value
	## of the particle to that digitized index of a new multi-dimensional array of size the same as bin number
	binnedyvalues3 = [[] for x in range(len(bin_edges3)-1)]
	binnedyvalues33 = [[] for x in range(len(bin_edges3)-1)]
	n3=0
	n33=0
	print 'Checking each particle...'
	for i in range(numparticles3):
		############ Before proceeding, check if we want to restrict data to a cross-section or axis ###############
		if args.remove:
			## Only add the particles to the bin if they are within a desired tolerance (calculated at start of file)
			## Check whether to restrict to a plane or axis, set the checking value accordingly
			if args.remove == 'x':
				checkpoints3 = xpositions3
			elif args.remove == 'y':
				checkpoints3 = ypositions3
			elif args.remove == 'z':
				checkpoints3 = zpositions3
			elif args.remove == 'xa':
				checkpoints3 = np.sqrt(ypositions3**2 + zpositions3**2)
			elif args.remove == 'ya':
				checkpoints3 = np.sqrt(xpositions3**2 + zpositions3**2)
			elif args.remove == 'za':
				checkpoints3 = cylindricalradii2
			elif args.remove == 'charles':
				checkpoints3 = zpositions3
				checkpoints33 = cylindricalradii3
	
			## Add particle's y value to the corresponding bin only if it is within the desired tolerance. Otherwise discard it.
			if np.abs(checkpoints3[i]) < tolerance3:
				binnedyvalues3[digitizedradii3[i]-1].append(yvalues3[i])
			else: n3=n3+1
	
			# Do this part only if we are plotting a second quantity
			if args.remove == 'charles':
				if np.abs(checkpoints33[i]) < tolerance3:
					binnedyvalues33[digitizedradii3[i]-1].append(yvalues3[i])
				else: n33=n33+1
	
		## If we are not restricting the data points, simply add them to the corresponding bins
		else: binnedyvalues3[digitizedradii3[i]-1].append(yvalues3[i])
	
		if i == int(numparticles3/4): print '25% complete'
		if i == int(numparticles3/2): print '50% complete'
		if i == int(3*numparticles3/4): print '75% complete'
	
	##############################################################################################################
	
	if args.remove: print 'Omitted {0} particles of {1}'.format(n3,numparticles3)
	print 'Averaging Bins...'
	
	## We now have the y values grouped in the same order as the binned radii. Average the bins to get average
	## y value for each bin, something we can plot against enclosed mass at each bin
	binnedyvalueserror3 = [[],[]]
	for i in range(len(binnedyvalues3)):
		## Mask empty array values to make a smooth curve
		if np.array(binnedyvalues3[i]).any():
			averageyvaluei3 = np.average(binnedyvalues3[i])
			binnedyvalueserror3[0].append(averageyvaluei3 - min(binnedyvalues3[i]))
			binnedyvalueserror3[1].append(max(binnedyvalues3[i]) - averageyvaluei3)
			binnedyvalues3[i] = averageyvaluei3
		else:
			binnedyvalues3[i] = None
			binnedyvalueserror3[0].append(None)
			binnedyvalueserror3[1].append(None)
	binnedyvalues3 = np.array(binnedyvalues3)
	binnedyvalueserror3 = np.array(binnedyvalueserror3)
	mask3 = np.isfinite(binnedyvalues3.astype(np.double))
	
	# Do the same if we are plotting a second quantity
	if args.remove == 'charles':
		binnedyvalueserror33 = [[],[]]
		for i in range(len(binnedyvalues33)):
			## Mask empty array values to make a smooth curve
			if np.array(binnedyvalues33[i]).any():
				averageyvaluei33 = np.average(binnedyvalues33[i])
				binnedyvalueserror33[0] = np.append(binnedyvalueserror33[0], averageyvaluei33 - min(binnedyvalues33[i]))
				binnedyvalueserror33[1] = np.append(binnedyvalueserror33[1], max(binnedyvalues33[i]) - averageyvaluei33)
				binnedyvalues33[i] = averageyvaluei33
			else:
				binnedyvalues33[i] = None	
				binnedyvalueserror33[0] = np.append(binnedyvalueserror33[0], None).astype(np.double)
				binnedyvalueserror33[1] = np.append(binnedyvalueserror33[1], None).astype(np.double)
		binnedyvalues33 = np.array(binnedyvalues33)
		binnedyvalueserror33 = np.array(binnedyvalueserror33)
		mask33 = np.isfinite(binnedyvalues33.astype(np.double))

	print 'Plotting...'
	
	## Now determine what to plot on the x axis, radii or enclosed mass
	## if mass, use enclosed mass at the right bin edges for x
	## if radii, use the average between the bin edge to the left or right
	## TO DO: Add possibilities for subplots with some flag
	if args.x_axis == 'm':
		## TO DO: Use stats module to caluclate standard deviation instead of just max/min? Or a percentile?
	
		## Plot Enclosed Mass against y value (Density or Temperature)
		plt.figure(2)
		## TO DO: Add possibility to change units
		if args.remove == 'charles':
			plt.plot(enclosedmass3[mask3]/enclosedmass3[-1], binnedyvalues3[mask3], color='red', label='No Shock')
			plt.plot(enclosedmass3[mask33]/enclosedmass3[-1], binnedyvalues33[mask33], color='red', linestyle='--')
			plt.legend(loc='best')
		else:
			plt.errorbar(enclosedmass3[mask3]/enclosedmass3[-1], binnedyvalues3[mask3], yerr=[binnedyvalueserror3[0][mask3],binnedyvalueserror3[1][mask3]],
							capsize=3, color='black')

	elif args.x_axis == 'r':
		## Calculate average radii in each bin
		avgedges3 = []
		for i in range(len(bins3)-1):
			avgedges3.append((bins3[i]+bins3[i+1])/2.)
		plt.figure(2)
		plt.rc('text', usetex=True)
		plt.rc('font', family='serif')
		## TO DO: Add possibility to change units
		## TO DO: Add label that distinguishes whether ommitting or not
		if args.remove == 'charles':
			plt.plot(avgedges3[mask3], binnedyvalues3[mask3], color='red', label='No Shock')
			plt.plot(avgedges3[mask33], binnedyvalues33[mask33], color='red', linestyle='--')
			plt.legend(loc='best')
		else:
			plt.errorbar(avgedges3[mask3], binnedyvalues3[mask3], yerr=[binnedyvalueserror3[0][mask3],binnedyvalueserror3[1][mask3]],
							capsize=3, color='black')

plt.show()
