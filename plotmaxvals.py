#!/usr/bin/env python

########################################################################
## Plotting code written for Phantom Log Files
## Current version made for plotting white dwarf stars
##
## Author: Alex Bercik & Terrence Tricco, May 2018
########################################################################

import matplotlib.pyplot as plt
import numpy as np
from scipy import optimize as opt
import argparse
import traceback
import sys

## Parse CommandLine Input
parser = argparse.ArgumentParser(
         description='Display data from Phantom log file',
         epilog='Written by Alex Bercik & Terrence Tricco May 2018')
parser.add_argument('logfile',help='Desired logfile, ex. blast01.ev')
parser.add_argument('y_axis',help='What to plot on the y-axis. [x] (Max x Pos),'\
                    '[y] (Max y Pos), [z] (Max z Pos), [m] (Max Particle mass), [h] (Max smooth length),'\
                    '[d] (Max Density), [vx] (Max x vel), [vy] (Max y vel), [vz] (Max z vel), [u] (Max therm E)'\
                    '[t] (Max Temp)')
args = parser.parse_args()

#### TO DO: Add unit conversions
#### TO DO: Add other quantities

## Read information from maxvals file into python
try:
	logfile = args.logfile
	time, x, y, z, mass, h, d, vx, vy, vz, u, t, pressure, alpha, divv, poten = np.loadtxt(logfile, unpack=True)
except:
	error = traceback.format_exc()
	print '\n[ERROR]: {0}'.format(error)
	sys.exit()

## Set the proper y variable to plot
## TO DO: Set a tag for the graph title and labels
if args.y_axis == 'x':
    yaxis = x
elif args.y_axis == 'y':
    yaxis = y
elif args.y_axis == 'z':
    yaxis = z
elif args.y_axis == 'm':
    yaxis = m
elif args.y_axis == 'h':
    yaxis = h
elif args.y_axis == 'd':
    yaxis = d
elif args.y_axis == 'vx':
    yaxis = vx
elif args.y_axis == 'vy':
    yaxis = vy
elif args.y_axis == 'vz':
    yaxis = vz
elif args.y_axis == 'u':
    yaxis = u
elif args.y_axis == 't':
    yaxis = t
elif args.y_axis == 'rmsmach':
    y = rmsmach
elif args.y_axis == 'vrms':
    y = vrms
elif args.y_axis == 'xcom':
    y = xcom
elif args.y_axis == 'ycom':
    y = ycom
elif args.y_axis == 'zcom':
    y = zcom
elif args.y_axis == 'alphamax':
    y = alphamax
elif args.y_axis == 'totlum':
    y = totlum
    
    
plt.figure(1)
plt.rc('text', usetex=True)
plt.rc('font', family='serif')
plt.title(args.y_axis)
plt.xlabel(r'Time (code units)')
#plt.ylabel(r'$\rho$ (g $\text{cm}^{-3}$)')	
## TO DO: Verify these are the right units, add a variable label if not
plt.plot(time, yaxis)
plt.show()
