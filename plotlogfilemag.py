#!/usr/bin/env python

########################################################################
## Plotting code written for Phantom Log Files
## Current version made for plotting white dwarf stars
##
## Author: Alex Bercik & Terrence Tricco, May 2018
########################################################################

import matplotlib.pyplot as plt
import numpy as np
from scipy import optimize as opt
import argparse
import traceback
import sys

## NOTE: CODE IS MESSY. PLEASE CLEAN ME UP :(

## Parse CommandLine Input
parser = argparse.ArgumentParser(
         description='Display data from Phantom log file',
         epilog='Written by Alex Bercik & Terrence Tricco May 2018')
parser.add_argument('logfile',help='Desired logfile, ex. blast01.ev')
parser.add_argument('y_axis',help='What to plot on the y-axis. [ekin] (Kin E),'\
                    '[etherm] (Therm E), [emag] (Mag E), [epot] (Pot E), [etot] (Tot E), [eall] (all E)'\
                    '[totmom] (Tot Mom), [angtot] (Tot Ang Mom), [rho] (Max and Avg Density),'\
                    '[hdivBB] (Max and Avg hdivB/B), [divB] (max and average divB)')
parser.add_argument('-d','--seconddump',help='Desired second logfile, ex. blast01.ev')
args = parser.parse_args()

#### TO DO: Add unit conversions
#### TO DO: Add other quantities

## Read information from log file into python
try:
	logfile = args.logfile
	time, ekin, etherm, emag, epot, etot, totmom, angtot, rhomax, rhoave,  dt, totentrop, \
	rmsmach, vrms, xcom, ycom, zcom, alphamax, divBmax, divBave, hdivBBmax, hdivBBave, \
	betamax, betaave, betamin = np.loadtxt(logfile, skiprows=1, unpack=True)
except:
	error = traceback.format_exc()
	print '\n[ERROR]: {0}'.format(error)
	sys.exit()

ploty2 = False
plotall = False

if args.seconddump:
	try:
    		logfile2 = args.seconddump
        	time2, ekin2, etherm2, emag2, epot2, etot2, totmom2, angtot2, rhomax2, rhoave2,  dt2, totentrop2, \
        	rmsmach2, vrms2, xcom2, ycom2, zcom2, alphamax2, divBmax2, divBave2, hdivBBmax2, hdivBBave2, \
        	betamax2, betaave2, betamin2 = np.loadtxt(logfile2, skiprows=1, unpack=True)
	except:
       		error = traceback.format_exc()
        	print '\n[ERROR]: {0}'.format(error)
        	sys.exit()


## Set the proper y variable to plot
## TO DO: Set a tag for the graph title and labels
if args.y_axis == 'ekin':
    y = ekin
elif args.y_axis == 'etherm':
    y = etherm
elif args.y_axis == 'emag':
	y = emag
	if args.seconddump:
                second=emag2
elif args.y_axis == 'epot':
    y = epot
elif args.y_axis == 'etot':
    y = etot
elif args.y_axis == 'totmom':
    y = totmom
elif args.y_axis == 'angtot':
    y = angtot
elif args.y_axis == 'rho':
	ploty2 = True
	y = rhomax
	y2 = rhoave
elif args.y_axis == 'totentrop':
    y = totentrop
elif args.y_axis == 'rmsmach':
    y = rmsmach
elif args.y_axis == 'vrms':
    y = vrms
elif args.y_axis == 'xcom':
    y = xcom
elif args.y_axis == 'ycom':
    y = ycom
elif args.y_axis == 'zcom':
    y = zcom
elif args.y_axis == 'alphamax':
    y = alphamax
elif args.y_axis == 'totlum':
    y = totlum
elif args.y_axis == 'hdivBB':
	ploty2 = True
	y = hdivBBmax
	y2 = hdivBBave
	if args.seconddump:
		second=hdivBBmax2
		second2=hdivBBave2
elif args.y_axis == 'divB':
        ploty2 = True
        y = divBmax
        y2 = divBave
        if args.seconddump:
                second=divBmax2
                second2=divBave2
elif args.y_axis == 'eall':
	plotall = True
	y = etot
        if args.seconddump:
                second=etot2


#Convert time to seconds
time = time*1.594E03    
if args.seconddump: time2=time2*1.594E03

plt.figure(1)
plt.rc('text', usetex=True)
plt.rc('font', family='serif')
plt.title(args.y_axis)
plt.xlabel(r'Time (s)')
if (not ploty2) and (not plotall):
	plt.plot(time, y, label='1ST LABEL')
        if args.seconddump:
                plt.plot(time2, second, label='2ND LABEL')
        	plt.legend(loc='best')
if ploty2:
	plt.plot(time, y, label='Max')
	plt.plot(time, y2, label='Avg')
        if args.seconddump:
                plt.plot(time2, second, label='Max2')
        	plt.plot(time2, second2, label='Avg2')
	plt.legend(loc='best')
if plotall:
	plt.plot(time, y, label='etot')
	plt.plot(time, etot[1] + etherm - np.average(etherm), label = 'etherm')
	plt.plot(time, etot[1] + emag - np.average(emag), label = 'emag')
	plt.plot(time, etot[1] + epot - np.average(epot), label = 'epot')
	plt.plot(time, etot[1] + ekin - np.average(ekin), label = 'ekin')
        if args.seconddump:
                plt.plot(time2, second, label='etot2')
                plt.plot(time2, etot[1] + etherm2 - np.average(etherm), label = 'etherm2')
	        plt.plot(time2, etot[1] + epot2 - np.average(epot), label = 'epot2')
        	plt.plot(time2, etot[1] + ekin2 - np.average(ekin), label = 'ekin2')
        plt.legend(loc='best')
if args.y_axis=='hdivBB':
	plt.ylim(ymin=0.0001)
if args.y_axis=='hdivBB' or args.y_axis=='emag':
	plt.yscale('log')


plt.show()

