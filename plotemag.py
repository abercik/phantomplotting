#!/usr/bin/env python

########################################################################
## Plotting code written for Phantom Output
## Current version made for plotting white dwarf stars
##
## Author: Alex Bercik & Terrence Tricco, May 2018
########################################################################

import matplotlib.pyplot as plt
import numpy as np
from scipy import optimize as opt
import argparse
import traceback
import sys
from libanalysis import PhantomAnalysis as pa

## Parse CommandLine Input
parser = argparse.ArgumentParser(
         description='Plot quantities ignoring unphysical fields from Phantom dump files',
         epilog='Written by Alex Bercik & Terrence Tricco August 2018')
parser.add_argument('first_dump',help='Desired dumpfile, ex. blast_00000')
parser.add_argument('last_dump',help='Desired dumpfile, ex. blast_00000')
parser.add_argument('reading_frequency',help='Number of dump files to skip, ex. 1')
parser.add_argument('y_axis',help='What to plot on the y-axis.',choices=['emag'])
parser.add_argument('-r','--remove',choices=['physical'], help='remove points with unphysical magnetic fields (hdivBB > 0.01)')
parser.add_argument('-o','--original', help='plot original log file. Input logfile name, ex. blast01.ev')
args = parser.parse_args()

###### NOTE: Currently hard coded in to ignore magnetic field in phantomanalysis.py, must uncomment this line!!

## Split dump strings into name and number
firstname , firstnum = args.first_dump.split('_')
lastname , lastnum = args.last_dump.split('_')
if firstname != lastname:
	print '\n[ERROR]: Dump file names do not match!'
	sys.exit()

## Create a list of dumps to read
dumpfiles = []
firstnum = int(firstnum)
lastnum = int(lastnum)
freq = int(args.reading_frequency)
while firstnum <= lastnum:
	dumpfiles.append(firstname + '_' + str("%05d" % (firstnum,)))
	firstnum = firstnum + freq
print 'Completed list of Dumpfiles, preparing to read'

## Read dumpfiles into multidimensional arrays holding all particles AND dumpfiles
## First declare arrays, then extract information from dumpfiles
times = np.array([])
numparticles = np.array([])
xpositions = np.array([])
ypositions = np.array([])
zpositions = np.array([])
smoothinglengths = np.array([])
xvelocities = np.array([])
yvelocities = np.array([])
zvelocities = np.array([])
thermenergies = np.array([])
temperatures = np.array([])
xmags = np.array([])
ymags = np.array([])
zmags = np.array([])
divBs = np.array([])

for dumpfile in dumpfiles:
	print dumpfile
	dump = pa(dumpfile)
## Extract information from dumpfile
## Make first entry into arrays, every subsequent entry is stacked ontop (2-dim arrays)
	if dumpfile == args.first_dump:
		mass = dump.massofgas
		times = np.append(times, np.array(dump.time))
		numparticles = np.append(numparticles, np.array(dump.npart))
		xpositions = np.append(xpositions, np.array(dump.xyzh[0]))
		ypositions = np.append(ypositions, np.array(dump.xyzh[1]))
		zpositions = np.append(zpositions, np.array(dump.xyzh[2]))
		smoothinglengths = np.append(smoothinglengths, np.array(dump.xyzh[3]))
#		xvelocities = np.append(xvelocities, np.array(dump.vxyz[0]))
#		yvelocities = np.append(yvelocities, np.array(dump.vxyz[1]))
#		zvelocities = np.append(zvelocities, np.array(dump.vxyz[2]))
		thermenergies = np.append(thermenergies, np.array(dump.utherm))
#		temperatures = np.append(temperatures, np.array(dump.temperature))
		xmags = np.append(xmags, dump.bxyz[0])
		ymags = np.append(ymags, dump.bxyz[1])
		zmags = np.append(zmags, dump.bxyz[2])
		divBs = np.append(divBs, np.abs(dump.divB))	
	else: 
		times = np.append(times, np.array(dump.time))
		xpositions = np.vstack((xpositions, np.array(dump.xyzh[0])))
		ypositions = np.vstack((ypositions, np.array(dump.xyzh[1])))
		zpositions = np.vstack((zpositions, np.array(dump.xyzh[2])))
		smoothinglengths = np.vstack((smoothinglengths, np.array(dump.xyzh[3])))
#		xvelocities = np.vstack((xvelocities, np.array(dump.vxyz[0])))
#		yvelocities = np.vstack((yvelocities, np.array(dump.vxyz[1])))
#		zvelocities = np.vstack((zvelocities, np.array(dump.vxyz[2])))
		thermenergies = np.vstack((thermenergies, np.array(dump.utherm)))
#		temperatures = np.vstack((temperatures, np.array(dump.temperature)))
		xmags = np.vstack((xmags, dump.bxyz[0]))
		ymags = np.vstack((ymags, dump.bxyz[1]))
		zmags = np.vstack((zmags, dump.bxyz[2]))
		divBs = np.vstack((divBs, np.abs(dump.divB)))

hdivBBs = smoothinglengths*divBs/np.sqrt(np.power(xmags,2) + np.power(ymags,2) + np.power(zmags,2))
hfact = 1.2
densities = hfact**3 / np.power(smoothinglengths,3)
# NOTE: normally density includes mass (duh) but since we would multiply my mass in emag anyway it cancels - so ignore it
emags = 0.5 * (np.power(xmags,2) + np.power(ymags,2) + np.power(zmags,2)) / densities

'''
avgdivBs = [np.average(subarray) for subarray in hdivBBs]
maxdivBs = [max(subarray) for subarray in hdivBBs]

print len(avgdivBs)
print np.average(divBs[0])
print divBs[0]
print dump.divB

plt.figure(2)
plt.rc('text', usetex=True)
plt.rc('font', family='serif')
plt.plot(times*1594. , avgdivBs, color='red')
plt.plot(times*1594. , maxdivBs, color='blue')
plt.yscale('log')
plt.show()
'''

############ Before proceeding, check if we want to restrict data to only physical magnetic ###############
if args.remove: 
	totaldumps = len(hdivBBs)
	particles = len(hdivBBs[0])
	## The following omits particles each dump, but if we remove a particle we want it gone for the ENTIRE calculation
	'''
	## Define new lists that will hold the filtered data
	newhdivBBs = [[] for x in range(totaldumps)]
	newemags =  [[] for x in range(totaldumps)]
	print 'Checking each particle...'
	for dump in range(totaldumps):
		n = 0
		for i in range(particles):
			## Only add the particles to the bin if the magnetic field is physical (hdivBB < 0.01)
			## Add particle's y value to the corresponding bin only if it is within the desired tolerance. Otherwise discard it.
			if hdivBBs[dump][i] < 1.0:
				#newhdivBBs[dump] = np.append(newhdivBBs[dump], hdivBBs[dump][i])
				#newemags[dump] = np.append(newemags[dump], emags[dump][i])
				newhdivBBs[dump].append(hdivBBs[dump][i])
				newemags[dump].append(emags[dump][i])
			else: n=n+1	
		print 'Dump {0} / {1} : Omitted {2} particles of {3}'.format(dump + 1, totaldumps, n, particles)
		## Now sum everything per dump (total emag)
		newemags[dump] = np.sum(newemags[dump])	
	'''
	## This is the correct way to do it
	print 'Checking each particle...'
	newemags = emags.copy()
	newhdivBBs = hdivBBs.copy() ### NOTE: FIND A WAY TO SET A MASK, NOT JUST TO 0, THAT WAY I CAN CALCULATE THINGS WITH REMAINING VALUES
	n = 0
	particles = len(hdivBBs[0])
	## Start on first dumpfile, move forward
	for dump in range(totaldumps):
		## Check each particle
		for i in range(particles):
			## If the particle's magnetic field is unphysical (hdivBB < 0.01), set it to 0 for ALL dumps (cycle through all dumps)
			## Also set the hdivBB values to 0 so that all future checks pass (FIND WAY TO USE MASK!!!)
			if newhdivBBs[dump][i] > 0.1:
				for dump2 in range(totaldumps):
					newemags[dump2][i] = 0
					newhdivBBs[dump2][i] = 0
				n=n+1	
		print 'Dump {0} / {1} : So far Omitting {2} particles of {3}'.format(dump + 1, totaldumps, n, particles)
	## Now sum everything per dump (total emag)
	newemags = np.sum(newemags, axis=1)	
##############################################################################################################


plt.figure(1)
plt.rc('text', usetex=True)
plt.rc('font', family='serif')
plt.title('emag')
plt.plot(times*1594. , np.sum(emags, axis=1), label='No Removing')
plt.plot(times*1594. , newemags, label='With Removing')
plt.yscale('log')
plt.legend(loc='best')

plt.show()



