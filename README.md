# PhantomPlotting

This is a python script used to plot data from phantom, an SPH / MHD code for astrophysics (specifically I use wd setup)

pyanalysis must be compiled for plotthings.py to work

plotthings.py plots the binary dump files, plotemag.py plots multiple dump files, and plotlogfilemag.py & plotmaxvals.py plot output log files.

pyphantom_divB_code is a text file detialing necessary changes to PHANTOM fortan code and python scripts for plotemag.py to work

dipole_code is a text file detailing the necessary changes to PHANTOM fortran code to include dipole fields in binary moddump and setup

For more information on phantom see https://bitbucket.org/danielprice/phantom/wiki/Home
